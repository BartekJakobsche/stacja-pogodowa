# README #



### Stacja Pogody ###


### Opis ###
Stacja pogody oparta na mikrokomputerze Raspberry PI 0w
Rozszerzona o obsługę żaluzji.   

### Funkcjonalność   ###
- System Rasbian zainstalowany na wyżej wymienionej Raspberry PI. 
- Czujniki znane jako moduły do Arduino.
- Ekran wyświetlający informacje zbierane i przetwarzane przez czujniki.
- Sterowanie ustawieniem żaluzji w zależnści od nateżenia światła. 
### Technologia ###
- Python
- Biblioteka GPIO (obsługa czujników, napędu żaluzji, ekranu)
- Czujniki : Temperatury oraz wilgotności powietrza, opadów , natężenia światła  